import unittest
from unittest import mock
import time
from jim.config import PROTO, JIM


def presence_message(account_name=None):
    pass


class TestClient(unittest.TestCase):
    @mock.patch('time.time', mock.MagicMock(return_value=12345))
    def test_presence_message_default(self):
        data = {JIM.ACTION: PROTO.PRESENCE, JIM.TIME: time.time(),
                JIM.USER: {PROTO.ACCOUNT_NAME: 'Guest'}}
        self.assertEqual(data, presence_message())

    @mock.patch('time.time', mock.MagicMock(return_value=12345))
    def test_presence_message_custom_name(self):
        account_name = 'Custom_user'
        data = {JIM.ACTION: PROTO.PRESENCE, JIM.TIME: time.time(),
                JIM.USER: {PROTO.ACCOUNT_NAME: account_name}}
        self.assertEqual(data, presence_message(account_name))


if __name__ == '__main__':
    unittest.main()
