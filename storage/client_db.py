from collections import namedtuple
from datetime import datetime
from pymongo import MongoClient


Row = namedtuple('Row', ('user', 'status', 'is_friend'))


class ClientStorage:
    """Класс для работы с БД"""

    def __init__(self, dbname='example', server='localhost', port=27017):
        """

        :param path: путь до базы данных
        """
        self.client = MongoClient(server, port)
        database = self.client[dbname]
        # { user: username, status: online, is_friend: true }
        self.contacts = database.contacts
        # { sender: username, receiver: username, time: datetime, message: msg }
        self.history = database.history

    def add_to_history(self, sender, receiver, message, time):
        """
        Добавить в историю

        :param sender: отправитель
        :param receiver: получатель
        :param message: сообщение
        :param time: время
        :return: None
        """
        history = {
            "sender": sender,
            "receiver": receiver,
            "time": datetime.fromtimestamp(time),
            "message": message
        }
        self.history.insert_one(history)

    def get_contacts(self):
        """
        Получить всех пользователей

        :return: ContactList query
        """
        result = list()
        for contact in self.contacts.find():
            result.append(Row(contact['user'], contact['status'],
                              contact['is_friend']))
        return result

    def get_contacts_list(self):
        """
        Получить всех пользователей в виде списка

        :return: list
        """
        return self.get_contacts()

    def get_history(self, user, contact):
        """
        Получить всю историю сообщений между пользователями user и contact

        :param user: имя пользователя
        :param contact: имя собеседника
        :return: list(sender.user, receiver.user, message, time)
        """
        result = list()
        for i in self.history.find({"$or": [
            {"sender": user, "receiver": contact},
            {"sender": contact, "receiver": user}
        ]}):
            result.append((i['sender'], i['receiver'], i['message'], i['time']))
        return result

    def update_contacts(self, contacts: dict):
        """
        Обновить текущие контакты

        :param contacts: список всех контактов
        :return:
        """
        all_contacts = self.get_contacts()
        # Update current
        for user in all_contacts:
            if user.user in contacts.keys():
                self.contacts.update({'user': user.user},
                                   {
                    'user': user.user,
                    "status": contacts[user.user]['status'],
                    "is_friend": contacts[user.user]['is_friend']
                                   })
                contacts.pop(user.user, None)
            else:
                self.contacts.pop({"user": user.user})
        # Add new
        for user in contacts.keys():
            contact = {
                "user": user,
                "status": contacts[user]['status'],
                "is_friend": contacts[user]['is_friend']

            }
            self.contacts.insert_one(contact)


if __name__ == '__main__':
    storage = ClientStorage()
    # storage.contacts.delete_many({})
    # contacts = {'test': {'status': 'offline', 'is_friend': False},
    #             'test2': {'status': 'online', 'is_friend': False},
    #             'test1': {'status': 'offline', 'is_friend': False},
    #             'test0': {'status': 'online', 'is_friend': False},
    #             'test3': {'status': 'online', 'is_friend': False},
    #             'Guest': {'status': 'online', 'is_friend': False}}
    # storage.update_contacts(contacts)
    # date = datetime.now().timestamp()
    # message = ('Guest', 'test3', 'test', date)
    # storage.add_to_history(*message)
    # for i in storage.contacts.find():
    #     print(i)
    history = storage.get_history('test3', 'Guest')
    for i in history:
        print(i)
