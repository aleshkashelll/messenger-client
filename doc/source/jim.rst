jim package
===========

Submodules
----------

jim.config module
-----------------

.. automodule:: jim.config
   :members:
   :undoc-members:
   :show-inheritance:

jim.utils module
----------------

.. automodule:: jim.utils
   :members:
   :undoc-members:
   :show-inheritance:


Module contents
---------------

.. automodule:: jim
   :members:
   :undoc-members:
   :show-inheritance:
