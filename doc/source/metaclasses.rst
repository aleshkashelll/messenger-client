metaclasses package
===================

Submodules
----------

metaclasses.meta module
-----------------------

.. automodule:: metaclasses.meta
   :members:
   :undoc-members:
   :show-inheritance:


Module contents
---------------

.. automodule:: metaclasses
   :members:
   :undoc-members:
   :show-inheritance:
