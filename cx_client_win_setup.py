from cx_Freeze import setup, Executable

executables = [Executable('client.py', base='Win32GUI',
                          targetName='client.exe')]

build_exe_options = {"packages": ["sqlalchemy"]}

setup(name='MessageClient',
      version='0.0.1',
      description='GeekBrains message client!',
      options={
          "build_exe": build_exe_options
        },
      executables=executables)
