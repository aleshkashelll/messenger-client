import os
import time
import logging
import threading
import binascii
import json
import asyncio
from socket import socket, AF_INET, SOCK_STREAM

from PyQt5.QtCore import pyqtSignal, QObject
from Crypto.PublicKey import RSA
from Crypto.Cipher import PKCS1_OAEP

from jim.config import LOGGING_LEVEL, PROTO, JIM
from jim.utils import send_message, get_message
from metaclasses.meta import ClientVerifier
from storage.client_db import ClientStorage


DEBUG = False
logger = logging.getLogger('client')
logger.setLevel(logging.DEBUG)
if DEBUG:
    formatter = logging.Formatter("%(asctime)-25s %(filename)-30s "
                                  "%(levelname)-10s %(message)s")
    console = logging.StreamHandler()
    console.setLevel(LOGGING_LEVEL)
    console.setFormatter(formatter)
    logger.addHandler(console)


class MessageClient(QObject):
    """Класс, содержащий логику работы клиента."""
    __metaclass__ = ClientVerifier
    """Проверка сетевых методов клиента"""
    updateHistory = pyqtSignal()
    """Сигнал интерфейсу о необходимости изменения истории"""
    session_key = None
    """Симметричный ключ сессии"""

    def __init__(self, name, address, port):
        super().__init__()
        self._init_net(address, port)
        self._init_encryption()
        self.name = name
        self.auth_data = {PROTO.USERNAME: self.name,
                          PROTO.PASSWORD: "Some pass"}
        eloop = asyncio.get_event_loop()
        try:
            task_obj = eloop.create_task(self.register_on_server())
            eloop.run_until_complete(task_obj)
        finally:
            eloop.close()
        if not task_obj.result():
            logger.error(f"Can't register on server")
            exit(1)
        self._receiver = threading.Thread(target=self.process_message)
        self._receiver.daemon = True
        self.contacts_updated = False
        logger.debug("Starting initialize storage")
        self.storage = ClientStorage(f'{self.name}')
        logger.debug(f'Initialize client done')

    def _init_net(self, address, port):
        """Подключение к серверу"""
        self._socket = socket(AF_INET, SOCK_STREAM)
        try:
            self._socket.connect((address, port))
            logger.info(f'Try connect to {address}:{port}')
        except ConnectionRefusedError:
            logger.error(f'Connection to {address}:{port} refused')
            exit(1)

    def _init_encryption(self):
        """Инициализация RSA ключей для начала обмена информацией с сервером"""
        key = RSA.generate(2048)
        self.private_key = key.export_key()
        self.public_key = key.publickey().export_key()

    def add_to_history(self, message):
        """Записать сообщение в БД"""
        logger.debug(f'Add to history: {message[JIM.SENDER]}, '
                     f'{message[JIM.RECEIVER]}, {message[PROTO.MESSAGE_TEXT]},'
                     f'{message[JIM.TIME]})')
        self.storage.add_to_history(message[JIM.SENDER], message[JIM.RECEIVER],
                                    message[PROTO.MESSAGE_TEXT],
                                    message[JIM.TIME])

    def create_message(self, receiver, text):
        """Создание сообщения по шаблону"""
        message = {
            JIM.ACTION: PROTO.MESSAGE,
            JIM.TIME: time.time(),
            JIM.SENDER: self.name,
            JIM.RECEIVER: receiver,
            PROTO.MESSAGE_TEXT: text
        }
        logger.debug(f'Create message: {message}')
        return message

    def get_history(self, sender, receiver):
        """Получить историю сообщений между клиентами"""
        return self.storage.get_history(sender, receiver)

    def process_message(self):
        """Основной цикл обработки сообщений"""
        _storage = ClientStorage(f'{self.name}')
        while True:
            message = get_message(self._socket)
            if not message:
                continue
            try:
                if message[JIM.ACTION] == PROTO.MESSAGE and \
                        self.name == message[JIM.RECEIVER]:
                    logger.debug(f'Message from {message[JIM.SENDER]}: '
                                 f'{message[PROTO.MESSAGE_TEXT]}')
                    _storage.add_to_history(message[JIM.SENDER],
                                            message[JIM.RECEIVER],
                                            message[PROTO.MESSAGE_TEXT],
                                            message[JIM.TIME])
                    logger.debug(f'Send a signal')
                    self.updateHistory.emit()
                elif message[JIM.ACTION] == PROTO.UPDATE_CONTACTS:
                    self.refresh_contacts(message[JIM.CONTACTS])
                elif message[JIM.ACTION] == PROTO.EXIT:
                    logger.info(f'Get exit message')
                    break
            except KeyError:
                logger.warning(f'Wrong message format: {message}')

    async def register_on_server(self):
        """Регистрация на сервере и получение сессионного ключа"""
        # Инициируем обмен ключами, отправляем открытый rsa ключ
        key_exchange_message = self.service_message(PROTO.KEY_EXCHANGE)
        send_message(sock=self._socket, data=key_exchange_message,
                     key=self.session_key)
        # От сервера получаем временное имя, зашифрованные сессионный ключ и iv
        key_exchange_answer = get_message(self._socket)
        tmp_name = key_exchange_answer[JIM.RECEIVER]
        # Расшифровываем
        hex_key = key_exchange_answer[JIM.KEY]
        logger.debug(f'Hex key: {hex_key}')
        byte_key = binascii.unhexlify(hex_key)
        logger.debug(f'Byte key: {byte_key}')
        cipher_rsa = PKCS1_OAEP.new(RSA.import_key(self.private_key))
        key_dict = json.loads(cipher_rsa.decrypt(byte_key))
        logger.debug(f'Getted keys: {key_dict}')
        self.session_key = {'session_key': binascii.unhexlify(
            key_dict['session_key']),
                            'iv': binascii.unhexlify(key_dict['iv'])}
        # Авторизуемся
        message = self.service_message(PROTO.PRESENCE, name=tmp_name,
                                       auth_data=self.auth_data)
        logger.debug(f'Presence message: {message}')
        send_message(self._socket, data=message, key=self.session_key)
        answer = get_message(self._socket, self.session_key)
        logger.debug(f'Server answer: {answer}')
        if answer:
            logger.info(f'Connection established')
            return True
        else:
            print("Некорректное сообщение от сервера")
            logger.info('Incorrect message from server')
            return False

    def refresh_contacts(self, contacts):
        """Обновление информации о контактах в БД"""
        self.storage.update_contacts(contacts)

    def initial_contacts(self):
        """Первоначальное обновление контактов (до старта интерфейса)"""
        message = self.service_message(PROTO.GET_CONTACTS)
        logger.debug(f'Update message: {message}')
        send_message(self._socket, message, key=self.session_key)
        answer = get_message(self._socket)
        logger.debug(f'Server answer: {answer}')
        try:
            if answer and answer[PROTO.RESPONSE] == 202:
                self.refresh_contacts(answer[JIM.CONTACTS])
                return True
        except KeyError:
            logger.error(f"Update contacts error. Answer: {answer}")
            return False
        return False

    def run(self):
        """Запуск рабочих потоков клиента"""
        for i in range(5):
            if self.initial_contacts():
                self.contacts_updated = True
                break
        if not self.contacts_updated:
            logger.error(f"Can't get contact list")
            exit(1)
        logger.debug(f'Contacts updated')
        logger.info(f'Started client: {self.name}')
        self._receiver.start()
        # self._sender.start()
        while True:
            if not self._receiver.is_alive():
                logger.debug(f"Receiver is dead")
                exit(0)
            time.sleep(2)

    def send(self, message_type, message=None):
        """Отправка сообщений серверу"""
        if message_type == PROTO.GET_CONTACTS:
            send_message(self._socket,
                         self.service_message(PROTO.GET_CONTACTS),
                         key=self.session_key)
        elif message_type == PROTO.MESSAGE:
            send_message(self._socket, message, key=self.session_key)
        elif message_type == PROTO.ADD_CONTACT:
            data = self.service_message(PROTO.ADD_CONTACT,
                                        friend=message[JIM.FRIEND])
            send_message(self._socket, data, key=self.session_key)
        elif message_type == PROTO.DEL_CONTACT:
            data = self.service_message(PROTO.DEL_CONTACT,
                                        friend=message[JIM.FRIEND])
            send_message(self._socket, data, key=self.session_key)
        elif message_type == PROTO.EXIT:
            send_message(self._socket, self.service_message(PROTO.EXIT),
                         key=self.session_key)
            logger.info('User command exit')
            time.sleep(1)
            # exit(0)

    def service_message(self, action, name=None, auth_data=None, friend=None):
        """Генерация сервисных сообщений в зависимости от типа"""
        if action == PROTO.KEY_EXCHANGE:
            return {
                JIM.ACTION: PROTO.KEY_EXCHANGE,
                JIM.TIME: time.time(),
                JIM.SENDER: binascii.hexlify(os.urandom(32)).decode('ascii'),
                JIM.KEY: binascii.hexlify(self.public_key).decode('ascii')
            }
        elif action == PROTO.PRESENCE:
            return {
                JIM.ACTION: action,
                JIM.TIME: time.time(),
                JIM.SENDER: name,
                JIM.AUTH_DATA: auth_data
            }
        elif action == PROTO.ADD_CONTACT:
            return {
                JIM.ACTION: action,
                JIM.TIME: time.time(),
                JIM.SENDER: self.name,
                JIM.FRIEND: friend
            }
        elif action == PROTO.DEL_CONTACT:
            return {
                JIM.ACTION: action,
                JIM.TIME: time.time(),
                JIM.SENDER: self.name,
                JIM.FRIEND: friend
            }
        else:
            return {
                JIM.ACTION: action,
                JIM.TIME: time.time(),
                JIM.SENDER: self.name
            }
