import os
import logging
import threading
from queue import Queue
from functools import partial

from kivy.app import App
from kivy.clock import Clock
from kivy.lang import Builder
from kivy.uix.behaviors import FocusBehavior
from kivy.uix.gridlayout import GridLayout
from kivy.uix.label import Label
from kivy.uix.recycleboxlayout import RecycleBoxLayout
from kivy.uix.recycleview import RecycleView
from kivy.uix.recycleview.layout import LayoutSelectionBehavior
from kivy.uix.recycleview.views import RecycleDataViewBehavior
from kivy.uix.widget import Widget
from kivy.properties import NumericProperty, ReferenceListProperty, \
    BooleanProperty
from kivy.vector import Vector
from kivy.uix.boxlayout import BoxLayout
from kivy.uix.button import Button
from kivy.uix.textinput import TextInput
from kivy import Config

from jim.config import LOGGING_LEVEL, PROTO, JIM

# Config.set('graphics', 'width', '1200')
# Config.set('graphics', 'height', '800')
Config.set('graphics', 'minimum_width', '800')
Config.set('graphics', 'minimum_height', '600')


DEBUG = False
logger = logging.getLogger('client')
logger.setLevel(logging.DEBUG)
if DEBUG:
    formatter = logging.Formatter("%(asctime)-25s %(filename)-30s "
                                  "%(levelname)-10s %(message)s")
    console = logging.StreamHandler()
    console.setLevel(LOGGING_LEVEL)
    console.setFormatter(formatter)
    logger.addHandler(console)


class SelectableRecycleBoxLayout(FocusBehavior, LayoutSelectionBehavior,
                                 RecycleBoxLayout):
    ''' Adds selection and focus behaviour to the view. '''


class SelectableLabel(RecycleDataViewBehavior, GridLayout):
    ''' Add selection support to the Label '''
    index = None
    selected = BooleanProperty(False)
    selectable = BooleanProperty(True)
    cols = 3

    def get_nodes(self):
        nodes = self.get_selectable_nodes()
        if self.nodes_order_reversed:
            nodes = nodes[::-1]
        if not nodes:
            return None, None

        selected = self.selected_nodes
        if not selected:  # nothing selected, select the first
            self.select_node(nodes[0])
            return None, None

        if len(nodes) == 1:  # the only selectable node is selected already
            return None, None

        last = nodes.index(selected[-1])
        self.clear_selection()
        return last, nodes

    def refresh_view_attrs(self, rv, index, data):
        ''' Catch and handle the view changes '''
        self.index = index
        self.label1_text = str(data['label1']['text'])
        self.label2_text = data['label2']['text']
        self.label3_text = data['label3']['text']
        return super(SelectableLabel, self).refresh_view_attrs(
            rv, index, data)

    def on_touch_down(self, touch):
        ''' Add selection on touch down '''
        if super(SelectableLabel, self).on_touch_down(touch):
            return True
        if self.collide_point(*touch.pos) and self.selectable:
            return self.parent.select_with_touch(self.index, touch)

    def apply_selection(self, rv, index, is_selected):
        ''' Respond to the selection of items in the view. '''
        self.selected = is_selected
        if is_selected:
            print("selection changed to {0}".format(rv.data[index]))
        else:
            print("selection removed for {0}".format(rv.data[index]))


class RV(RecycleView):

    def __init__(self, **kwargs):
        super(RV, self).__init__(**kwargs)
        data = [(i, f'name{i}', f'{"friend" if i%3 == 0 else ""}') for i in range(50)]
        # data = [
        #     (1, 'name1', 'friend'),
        #     (2, 'name2', ''),
        #     (3, 'name3', ''),
        #     (4, 'name4', 'friend')
        # ]
        self.size_hint = (1, 1)
        self.update_data(data)

    def get_selected(self):
        print(dir(self.view_adapter))
        print(20*"+")
        for i in dir(self):
            print(i)

    def update_data(self, data):
        self.data = [
            {'label1': {'text': x[0]},
             'label2': {'text': x[1]},
             'label3': {'text': x[2]}} for x in data]


class MainApp(App):
    current_selection = None

    def __init__(self, client=None, **kwargs):
        super().__init__(**kwargs)
        self.message_queue = Queue()
        self.current_contact = None
        self.messageHistory = list()
        self.client = client

    def add_contact(self):
        self.message_queue.put({"type": PROTO.ADD_CONTACT, "message": {
            JIM.FRIEND: self.current_selection
        }})

    def build(self):
        main_layout = BoxLayout(orientation='horizontal')
        left_layout = BoxLayout(orientation='vertical')
        right_layout = BoxLayout(orientation='vertical')
        send_layout = BoxLayout(orientation='horizontal', size=(50, 50),
                                size_hint=(1, None))
        friend_btn_layout = BoxLayout(orientation='horizontal', size_hint=(1, None))

        # Contacts
        self.contact_list = RV()
        btn_add_friend = Button(text='Add friend', size=(50, 50),
                                size_hint=(1, None))
        btn_del_friend = Button(text='Del friend', size=(50, 50),
                                size_hint=(1, None))
        friend_btn_layout.add_widget(btn_add_friend)
        friend_btn_layout.add_widget(btn_del_friend)
        left_layout.add_widget(self.contact_list)
        left_layout.add_widget(friend_btn_layout)


        # History and send message
        txt_history = TextInput(halign='left', readonly=True)
        self.txt_send = TextInput(halign='left', size=(50, 50), size_hint=(1, None))
        btn_send = Button(text='Send', size=(50, 50), size_hint=(None, None),
                          on_press=lambda a:self.send_message())

        send_layout.add_widget(self.txt_send)
        send_layout.add_widget(btn_send)
        right_layout.add_widget(txt_history)
        right_layout.add_widget(send_layout)

        main_layout.add_widget(left_layout)
        main_layout.add_widget(right_layout)

        return main_layout

    def send_message(self, **kwargs):
        """Получение сообщения из интерфейса и отправка на сервер"""
        print(self.txt_send.text)
        # print(self.contact_list.get_selected())
        # text = self.txt_send.text
        # data = [
        #     (1, 'name1', 'friend'),
        #     (2, 'name2', ''),
        #     (3, 'name3', ''),
        #     (4, 'name4', 'friend')
        # ]
        # self.contact_list.update_data(data)
        # if text and self.current_contact:
        #     if text == "/exit":
        #         self.message_queue.put({"type": PROTO.EXIT, "message": None})
        #     else:
        #         message = self.client.create_message(
        #             receiver=self.current_contact, text=text)
        #         self.message_queue.put({"type": PROTO.MESSAGE,
        #                                 "message": message})
        #         logger.debug(f"Message sent")
        #         self.messageHistory.append(f"<div>Me: {text}</div>")
        #         self.textSend.clear()
        #         self.client.add_to_history(message)
        #         logger.debug(f"Message added to history")


if __name__ == '__main__':
    MainApp().run()
