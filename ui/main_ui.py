import os
import logging
import threading
from queue import Queue
from functools import partial

from PyQt5.QtWidgets import QHeaderView, QAbstractItemView, QAction, \
    QToolButton
from PyQt5 import QtWidgets
from PyQt5.QtCore import QTimer, pyqtSlot
from PyQt5.QtGui import QStandardItemModel, QStandardItem, QIcon, \
    QTextCharFormat, QFont

from jim.config import LOGGING_LEVEL, PROTO, JIM
from .client_main import Ui_MainWindow
from .change_avatar import AvatarWindow


DEBUG = False
logger = logging.getLogger('client')
logger.setLevel(logging.DEBUG)
if DEBUG:
    formatter = logging.Formatter("%(asctime)-25s %(filename)-30s "
                                  "%(levelname)-10s %(message)s")
    console = logging.StreamHandler()
    console.setLevel(LOGGING_LEVEL)
    console.setFormatter(formatter)
    logger.addHandler(console)


class Interface(QtWidgets.QMainWindow, Ui_MainWindow):
    """Класс интрефейса клиента"""
    current_contact = None
    """Активный собеседник"""
    current_selection = None
    """Выделенная строка"""
    timer = QTimer()
    """Таймер для обновления информации о контактах в БД"""
    timer_update_contact = QTimer()
    """Таймер обновления контактов в интерфейсе"""
    smile_path = 'files/smiles'
    """Путь до смайлов"""

    def __init__(self, client=None, parent=None):
        self.message_queue = Queue()
        self.client = client
        QtWidgets.QMainWindow.__init__(self, parent)
        self.setupUi(self)
        self.smiles = [
            {'path': 'zipper_mouth_face_256_1.gif', 'title': 'zipper_mouth'},
            {'path': 'zany_face_256_1.gif', 'title': 'zany'},
            {'path': 'thumbs_up_sign_256_1.gif', 'title': 'thumbs_up'},
            {'path': 'tears_of_joy_256_2.gif', 'title': 'tears_of_joy'},
            {'path': 'party_face_256_1.gif', 'title': 'party_face'},
            {'path': 'loudly_crying_face_256_1.gif', 'title': 'crying'},
            {'path': 'hot_face_256_1.gif', 'title': 'hot_face'},
            {'path': 'face_with_open_mouth_vomiting_256_1.gif', 'title':
                'mouth_vomit'},
            {'path': 'face_throwing_a_kiss_256_1.gif', 'title': 'kiss'},
            {'path': 'exploding_head_256_1.gif', 'title': 'expoding_head'},
            {'path': 'cold_face_256_1.gif', 'title': 'cold_face'},
            {'path': 'clapping_hands_256_1.gif', 'title': 'clapping'}
        ]
        # ui_path = os.path.dirname(os.path.abspath(__file__))
        # uic.loadUi(os.path.join(ui_path, 'files/client_main.ui'), self)
        self.message_thread = threading.Thread(
            target=self.process_queue_message)
        self.prepare()
        self.interface_patch()
        self.make_connections()

    def add_contact(self):
        self.message_queue.put({"type": PROTO.ADD_CONTACT, "message": {
            JIM.FRIEND: self.current_selection
        }})

    def change_avatar(self):
        window = AvatarWindow(self)
        window.show()

    def del_contact(self):
        self.message_queue.put({"type": PROTO.DEL_CONTACT,
                                "message": {
                                    JIM.FRIEND: self.current_selection}})

    def interface_patch(self):
        """Правки интерфейса перед запуском"""
        # Format buttons
        bold_action = QAction(QIcon.fromTheme('format-text-bold'), 'bold', self)
        italic_action = QAction(QIcon.fromTheme('format-text-italic'),
                                 'italic', self)
        underline_action = QAction(QIcon.fromTheme('format-text-underline'),
                                   'underline', self)
        bold_action.triggered.connect(lambda: self.format_text(type='bold'))
        italic_action.triggered.connect(lambda: self.format_text(type='italic'))
        underline_action.triggered.connect(lambda: self.format_text(
            type='underline'))
        bold_btn = QToolButton(self)
        italic_btn = QToolButton(self)
        underline_btn = QToolButton(self)
        bold_btn.setDefaultAction(bold_action)
        italic_btn.setDefaultAction(italic_action)
        underline_btn.setDefaultAction(underline_action)
        self.smileLayout.addWidget(bold_btn)
        self.smileLayout.addWidget(italic_btn)
        self.smileLayout.addWidget(underline_btn)

        # Smiles
        for i in self.smiles:
            smile_action = QAction(QIcon(os.path.join(self.smile_path,
                                                      i['path'])),
                                   i['title'], self)
            smile_action.triggered.connect(partial(self.print_smile,
                                                   i['title']))
            tmp_button = QToolButton(self)
            tmp_button.setDefaultAction(smile_action)
            self.smileLayout.addWidget(tmp_button)
        spacer_item = QtWidgets.QSpacerItem(20, 5,
                                            QtWidgets.QSizePolicy.Expanding,
                                            QtWidgets.QSizePolicy.Expanding)
        self.smileLayout.addItem(spacer_item)
        self.contactTable.setSelectionBehavior(QAbstractItemView.SelectRows)
        self.setWindowTitle(f"Client: {self.client.name}")

    def format_text(self, type=None):
        format = QTextCharFormat()
        cursor = self.textSend.textCursor()
        current_format = cursor.charFormat()
        if type == 'bold':
            current_weight = current_format.fontWeight()
            if current_weight == QFont.Normal:
                format.setFontWeight(QFont.Bold)
            else:
                format.setFontWeight(QFont.Normal)
        elif type == 'italic':
            italic = current_format.fontItalic()
            format.setFontItalic(not italic)
        elif type == 'underline':
            underline = current_format.fontUnderline()
            format.setFontUnderline(not underline)
        cursor.mergeCharFormat(format)

    def make_connections(self):
        """Соединение событий с функциями"""
        self.contactTable.doubleClicked.connect(self.set_current_contact)
        self.contactTable.clicked.connect(self.set_current_selection)
        self.sendButton.clicked.connect(self.send_message)
        self.client.updateHistory.connect(self.show_message_history)
        self.addContact.clicked.connect(self.add_contact)
        self.delContact.clicked.connect(self.del_contact)
        self.change_avatar_action.triggered.connect(self.change_avatar)

    def set_current_contact(self, index):
        """Установить активного собеседника"""
        self.set_current_selection(index)
        self.current_contact = index.sibling(index.row(), 0).data()
        self.update_contact_list()
        self.show_message_history()

    def set_current_selection(self, index):
        self.current_selection = index.sibling(index.row(), 0).data()

    @pyqtSlot()
    def show_message_history(self):
        """Обновление истории с собеседником в интерфейсе"""
        logger.debug(f'Call show history')
        lines = self.client.get_history(self.client.name, self.current_contact)
        self.messageHistory.clear()
        for sender, receiver, message, message_time in lines:
            if sender == self.client.name:
                sender = "Me"
            self.messageHistory.append(f'<div>{sender}: {message}</div>')

    def show_message(self, value):
        """Добавление собственного сообщения в интерфейс"""
        self.messageHistory.append(f'{value[JIM.SENDER]}: '
                                   f'{value[PROTO.MESSAGE_TEXT]}')

    def prepare(self):
        """Подготовка потоков, таймера и запуск вспомогательных функций"""
        self.message_thread.daemon = True
        self.message_thread.start()
        self.timer.timeout.connect(lambda: self.message_queue.put(
            {"type": PROTO.GET_CONTACTS, "message": None}))
        self.timer.start(10000)
        self.client.send(message_type=PROTO.GET_CONTACTS)
        self.timer_update_contact.timeout.connect(self.update_contact_list)
        self.timer_update_contact.start(1000)
        self.update_contact_list()
        self.show_message_history()

    def print_smile(self, title):
        for i in self.smiles:
            if i['title'] == title:
                url = os.path.join(self.smile_path, i['path'])
                cursor = self.textSend.textCursor()
                cursor.insertHtml(f'<img src="{url}" width="20" />')
                # self.textSend.append(f'<img src="{url}" width="20" />')

    def process_queue_message(self):
        """Цикл обработки входящих сообщений"""
        while True:
            item = self.message_queue.get()
            if item is None:
                logger.debug(f'Getting None item in queue. '
                             f'Closing process_queue_message.')
                break
            if item['type'] == PROTO.EXIT:
                self.client.send(message_type=item['type'],
                                 message=item['message'])
                self.close()
                break
            try:
                self.client.send(message_type=item['type'],
                                 message=item['message'])
            except Exception as e:
                logger.debug(f'Exception on send message: {e}')
            self.message_queue.task_done()
        self.message_queue.task_done()

    def run(self):
        """Запуск логики клиента"""
        logger.info("Client started")
        self.client.run()

    def send_message(self):
        """Получение сообщения из интерфейса и отправка на сервер"""
        text = self.textSend.toHtml()
        if text and self.current_contact:
            if text == "/exit":
                self.message_queue.put({"type": PROTO.EXIT, "message": None})
            else:
                message = self.client.create_message(
                    receiver=self.current_contact, text=text)
                self.message_queue.put({"type": PROTO.MESSAGE,
                                        "message": message})
                logger.debug(f"Message sent")
                self.messageHistory.append(f"<div>Me: {text}</div>")
                self.textSend.clear()
                self.client.add_to_history(message)
                logger.debug(f"Message added to history")

    def update_contact_list(self):
        """Обновление списка контактов"""
        contacts = self.client.storage.get_contacts()
        headers = ['Login', 'Status', 'Friend']
        model = QStandardItemModel()
        model.setHorizontalHeaderLabels(headers)
        select_row = 0
        for index, user in enumerate(contacts):
            if index == 0 and not self.current_contact:
                self.current_contact = user.user
            login = user.user
            status = user.status
            is_friend = user.is_friend

            login = QStandardItem(login)
            login.setEditable(False)
            status = QStandardItem(status)
            status.setEditable(False)
            if is_friend:
                is_friend = QStandardItem('friend')
            else:
                is_friend = QStandardItem('')
            is_friend.setEditable(False)
            model.appendRow([login, status, is_friend])
            if self.current_selection:
                if self.current_selection == user.user:
                    select_row = index
        self.contactTable.setModel(model)
        self.contactTable.horizontalHeader().setSectionResizeMode(
            QHeaderView.Stretch)
        self.contactTable.selectRow(select_row)
