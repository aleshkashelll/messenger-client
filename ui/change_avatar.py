import sys
import os
import copy

from PIL import Image, ImageDraw
from PIL.ImageQt import ImageQt
from PyQt5 import QtCore, QtGui, QtWidgets
from PyQt5.QtWidgets import QToolBox, QHBoxLayout, QFileDialog


class AvatarWindow(QtWidgets.QDialog):
    height = 400
    width = 500
    icon_path = 'files/icons'
    image = None

    def __init__(self, parent=None, icon_path=None):
        if icon_path:
            self.icon_path = icon_path
        self.icons = {
            'black_white': 'black_white.svg',
            'color': 'color.svg',
            'sepia': 'sepia.png'
        }
        QtWidgets.QDialog.__init__(self, parent)
        self.setWindowTitle('Обновление аватара')
        self.setMinimumHeight(self.height)
        self.setMinimumWidth(self.width)
        self.init_ui()

    def init_ui(self):
        # Toolbar
        main_toolbar = QtWidgets.QToolBar(self)
        act_open = QtWidgets.QAction(QtGui.QIcon.fromTheme('document-open'),
                                        'open', self)
        act_color = QtWidgets.QAction(QtGui.QIcon(
            os.path.join(self.icon_path, self.icons['color'])), 'color', self
        )
        act_sepia = QtWidgets.QAction(QtGui.QIcon(
            os.path.join(self.icon_path, self.icons['sepia'])), 'sepia', self
        )
        act_black = QtWidgets.QAction(QtGui.QIcon(
            os.path.join(self.icon_path, self.icons['black_white'])),
                                      'black', self)
        act_open.triggered.connect(self.open_image)
        act_color.triggered.connect(self.picture_original)
        act_black.triggered.connect(self.picture_black_white)
        act_sepia.triggered.connect(self.picture_sepia)
        main_toolbar.addAction(act_open)
        main_toolbar.addAction(act_color)
        main_toolbar.addAction(act_black)
        main_toolbar.addAction(act_sepia)
        # Buttons
        ok_btn = QtWidgets.QPushButton(self)
        ok_btn.setText('OK')
        ok_btn.clicked.connect(self.close)
        cancel_btn = QtWidgets.QPushButton(self)
        cancel_btn.setText('Cancel')
        cancel_btn.clicked.connect(self.close)
        # Main field
        self.img_label = QtWidgets.QLabel(self)
        self.img_label.setMinimumWidth(self.width)
        self.img_label.setMinimumHeight(self.height)
        # Layers
        vbox = QtWidgets.QVBoxLayout(self)
        hbox_btn = QtWidgets.QHBoxLayout(self)

        hbox_btn.addWidget(ok_btn)
        hbox_btn.addWidget(cancel_btn)
        vbox.addWidget(main_toolbar)
        vbox.addWidget(self.img_label)
        vbox.addLayout(hbox_btn)

    def open_image(self):
        file_name = QFileDialog.getOpenFileName(self, 'Open file', '/home')[0]
        self.image = Image.open(file_name)
        width, height = self.image.size
        crop_width = int(width * 0.1)
        crop_height = int(height * 0.1)
        self.image = self.image.crop((crop_width, crop_height,
                                      width - crop_width, height - crop_height))
        self.put_to_label(self.image)

    def picture_black_white(self):
        width, height = self.image.size
        image = copy.deepcopy(self.image)
        draw = ImageDraw.Draw(image)
        pix = image.load()
        factor = 50
        for i in range(width):
            for j in range(height):
                a = pix[i, j][0]
                b = pix[i, j][1]
                c = pix[i, j][2]
                s = a + b + c
                if s > (((255 + factor) // 2) * 3):
                    a, b, c = 255, 255, 255
                else:
                    a, b, c = 0, 0, 0
                draw.point((i, j), (a, b, c))
        self.put_to_label(image)

    def picture_original(self):
        self.put_to_label(self.image)

    def picture_sepia(self):
        width, height = self.image.size
        image = copy.deepcopy(self.image)
        draw = ImageDraw.Draw(image)
        pix = image.load()

        depth = 30
        for i in range(width):
            for j in range(height):
                a = pix[i, j][0]
                b = pix[i, j][1]
                c = pix[i, j][2]
                S = (a + b + c)
                a = S + depth * 2
                b = S + depth
                c = S
                if (a > 255):
                    a = 255
                if (b > 255):
                    b = 255
                if (c > 255):
                    c = 255
                draw.point((i, j), (a, b, c))
        self.put_to_label(image)

    def put_to_label(self, img):
        width, height = img.size
        if self.width / width < self.height / height:
            ratio = self.width / width
        else:
            ratio = self.height / height
        image = img.resize((int(width * ratio), int(height * ratio)),
                           Image.ANTIALIAS)
        image = ImageQt(image.convert('RGBA'))
        pix_map = QtGui.QPixmap.fromImage(image)
        self.img_label.setPixmap(pix_map)


if __name__ == '__main__':
    app = QtWidgets.QApplication(sys.argv)
    window = AvatarWindow(icon_path='../files/icons')
    window.show()
    sys.exit(app.exec_())
