import sys
import argparse
import logging
import threading
import tempfile

from PyQt5 import QtWidgets

from jim.config import LOGGING_LEVEL, DEF_SERVER_ADDR, DEF_SERVER_PORT
from loggers import init_logging
from ui.main_ui import Interface
from engine.engine import MessageClient


init_logging(tempfile.gettempdir())


DEBUG = False
logger = logging.getLogger('client')
logger.setLevel(logging.DEBUG)
if DEBUG:
    formatter = logging.Formatter("%(asctime)-25s %(filename)-30s "
                                  "%(levelname)-10s %(message)s")
    console = logging.StreamHandler()
    console.setLevel(LOGGING_LEVEL)
    console.setFormatter(formatter)
    logger.addHandler(console)


def get_values():
    """Создание парсера параметов запуска"""
    parser = argparse.ArgumentParser()
    parser.add_argument('-a', '--address', help='Адрес сервера',
                        default=DEF_SERVER_ADDR)
    parser.add_argument('-p', '--port', help='Порт сервера',
                        default=DEF_SERVER_PORT)
    parser.add_argument('-n', '--name', help='Имя пользователя',
                        default='Guest')
    args = parser.parse_args()
    logger.info(f'Параметры командной строки: {args}')
    if args.port < 1024 and args.port > 65535:
        logger.error(f'Некорректный порт: {args}')
        print("Неккоректный порт. Введите порт в диапзоне 1024-65535")
        exit(-1)
    return args


def run_ui():
    """Подготовка и запуск основных компонентов программы"""
    args = get_values()
    client = MessageClient(args.name, args.address, args.port)
    app = QtWidgets.QApplication(sys.argv)
    window = Interface(client)
    window.show()
    t = threading.Thread(target=window.run)
    t.daemon = True
    t.start()
    sys.exit(app.exec_())


def test_ui():
    """Тестирование интерфейса без использования сети"""
    app = QtWidgets.QApplication(sys.argv)
    window = Interface()
    window.show()
    sys.exit(app.exec_())


if __name__ == '__main__':
    run_ui()
